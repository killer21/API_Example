package fr.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.model.User;

public interface IUserDao extends JpaRepository<User, Long> {

}

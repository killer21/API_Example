package fr.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import fr.model.Post;

public interface IPostDao extends JpaRepository<Post, Long> {

	@Query("SELECT u.posts FROM User u WHERE u.id = :id")
	List<Post> findAllPostsByUser(@Param("id") long id);
}

package fr.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import fr.model.Comment;

public interface ICommentDao extends JpaRepository<Comment, Long> {

	@Query("SELECT p.comments FROM Post p WHERE p.id = :id")
	List<Comment> findAllCommentsByPost(@Param("id") long id);

}

package fr.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.model.Trip;

public interface ITripDao extends JpaRepository<Trip, Long> {

}

package fr.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.dao.IPostDao;
import fr.model.Post;

@RestController
@RequestMapping("/api/posts/")
public class PostController {

	@Autowired
	private IPostDao repositoryPost;

	@RequestMapping("/{userId}/")
	public List<Post> getAll(@PathVariable("userId") long id) {
		return repositoryPost.findAllPostsByUser(id);
	}

	@RequestMapping("/{userId}/{id}")
	public Post getOne(@PathVariable("id") long id, @PathVariable("id") long userId) {
		// check user
		return repositoryPost.findOne(id);
	}

	@RequestMapping(value = "/{userId}/", method = RequestMethod.POST)
	public Object add(@Valid @RequestBody Post u, BindingResult br) {

		if (br.hasErrors()) {
			Map<String, Object> errors = new HashMap<String, Object>();
			errors.put("error", true);
			System.err.println(br.getFieldErrors());
			for (FieldError fe : br.getFieldErrors())
				errors.put(fe.getField(), fe.getDefaultMessage());

			return errors;
		}

		// check user

		return repositoryPost.save(u);
	}

	@RequestMapping(value = "/{userId}/{id}", method = RequestMethod.DELETE)
	public boolean delete(@PathVariable("id") long id) {
		Post u = repositoryPost.findOne(id);
		repositoryPost.delete(u);
		return true;
	}

	@RequestMapping(value = "/{userId}/{id}", method = RequestMethod.PUT)
	public Post update(@PathVariable("id") long id, @RequestBody Post u) {
		return repositoryPost.saveAndFlush(u);
	}
}

package fr.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.dao.IUserDao;
import fr.model.User;

@RestController
@RequestMapping("/api/users/")
public class UserController {

	@Autowired
	private IUserDao repositoryUser;

	@RequestMapping("/")
	public List<User> getAll() {
		return repositoryUser.findAll();
	}

	@RequestMapping("/{id}")
	public User getOne(@PathVariable("id") long id) {
		return repositoryUser.findOne(id);
	}

	@RequestMapping(value = "/", method = RequestMethod.POST)
	public Object add(@Valid @RequestBody User u, BindingResult br) {

		if (br.hasErrors()) {
			Map<String, Object> errors = new HashMap<String, Object>();
			errors.put("error", true);
			System.err.println(br.getFieldErrors());
			for (FieldError fe : br.getFieldErrors())
				errors.put(fe.getField(), fe.getDefaultMessage());

			return errors;
		}

		return repositoryUser.save(u);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public boolean delete(@PathVariable("id") long id) {
		User u = repositoryUser.findOne(id);
		repositoryUser.delete(u);
		return true;
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public User update(@PathVariable("id") long id, @RequestBody User u) {
		return repositoryUser.saveAndFlush(u);
	}

}

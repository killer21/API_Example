package fr.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.dao.ICommentDao;
import fr.model.Comment;

@RestController
@RequestMapping("/api/comments/")
public class CommentController {

	@Autowired
	private ICommentDao repositoryComment;

	@RequestMapping("/{postId}/")
	public List<Comment> getAll(@PathVariable("postId") long id) {
		return repositoryComment.findAllCommentsByPost(id);
	}

	@RequestMapping("/{userId}/{id}")
	public Comment getOne(@PathVariable("id") long id, @PathVariable("id") long userId) {
		// check user
		return repositoryComment.findOne(id);
	}

	@RequestMapping(value = "/{userId}/", method = RequestMethod.POST)
	public Object add(@Valid @RequestBody Comment u, BindingResult br) {

		if (br.hasErrors()) {
			Map<String, Object> errors = new HashMap<String, Object>();
			errors.put("error", true);
			System.err.println(br.getFieldErrors());
			for (FieldError fe : br.getFieldErrors())
				errors.put(fe.getField(), fe.getDefaultMessage());

			return errors;
		}

		// check user

		return repositoryComment.save(u);
	}

	@RequestMapping(value = "/{userId}/{id}", method = RequestMethod.DELETE)
	public boolean delete(@PathVariable("id") long id) {
		Comment u = repositoryComment.findOne(id);

		if (u == null)
			return false;

		repositoryComment.delete(u);
		return true;
	}

	@RequestMapping(value = "/{userId}/{id}", method = RequestMethod.PUT)
	public Comment update(@PathVariable("id") long id, @RequestBody Comment u) {
		return repositoryComment.saveAndFlush(u);
	}

}

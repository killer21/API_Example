package fr.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.dao.ITripDao;
import fr.model.Trip;

@RestController
@RequestMapping("/api/trips")
public class TripController {

	@Autowired
	ITripDao repositoryTrip;
	
	@RequestMapping("/")
	public List<Trip> getAll() {
		return repositoryTrip.findAll();
	}

	@RequestMapping("/{id}")
	public Trip getOne(@PathVariable("id") long id) {
		// check user
		return repositoryTrip.findOne(id);
	}

	@RequestMapping(value = "/", method = RequestMethod.POST)
	public Object add(@Valid @RequestBody Trip u, BindingResult br) {

		if (br.hasErrors()) {
			Map<String, Object> errors = new HashMap<String, Object>();
			errors.put("error", true);
			System.err.println(br.getFieldErrors());
			for (FieldError fe : br.getFieldErrors())
				errors.put(fe.getField(), fe.getDefaultMessage());

			return errors;
		}

		return repositoryTrip.save(u);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public boolean delete(@PathVariable("id") long id) {
		Trip u = repositoryTrip.findOne(id);

		if (u == null)
			return false;

		repositoryTrip.delete(u);
		return true;
	}

	@RequestMapping(value = "/{userId}/{id}", method = RequestMethod.PUT)
	public Trip update(@PathVariable("id") long id, @RequestBody Trip u) {
		return repositoryTrip.saveAndFlush(u);
	}

}

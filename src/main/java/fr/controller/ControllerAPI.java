package fr.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/")
public class ControllerAPI {

	@RequestMapping("/")
	public String helloMethod() {
		return "Hello";
	}

}
